<?php
/**
 * Podstawowa konfiguracja WordPressa.
 *
 * Ten plik zawiera konfiguracje: ustawień MySQL-a, prefiksu tabel
 * w bazie danych, tajnych kluczy i ABSPATH. Więcej informacji
 * znajduje się na stronie
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Kodeksu. Ustawienia MySQL-a możesz zdobyć
 * od administratora Twojego serwera.
 *
 * Ten plik jest używany przez skrypt automatycznie tworzący plik
 * wp-config.php podczas instalacji. Nie musisz korzystać z tego
 * skryptu, możesz po prostu skopiować ten plik, nazwać go
 * "wp-config.php" i wprowadzić do niego odpowiednie wartości.
 *
 * @package WordPress
 */

// ** Ustawienia MySQL-a - możesz uzyskać je od administratora Twojego serwera ** //
/** Nazwa bazy danych, której używać ma WordPress */
define('DB_NAME', 'animals');

/** Nazwa użytkownika bazy danych MySQL */
define('DB_USER', 'root');

/** Hasło użytkownika bazy danych MySQL */
define('DB_PASSWORD', '1');

/** Nazwa hosta serwera MySQL */
define('DB_HOST', 'localhost');

/** Kodowanie bazy danych używane do stworzenia tabel w bazie danych. */
define('DB_CHARSET', 'utf8');

/** Typ porównań w bazie danych. Nie zmieniaj tego ustawienia, jeśli masz jakieś wątpliwości. */
define('DB_COLLATE', '');

/**#@+
 * Unikatowe klucze uwierzytelniania i sole.
 *
 * Zmień każdy klucz tak, aby był inną, unikatową frazą!
 * Możesz wygenerować klucze przy pomocy {@link https://api.wordpress.org/secret-key/1.1/salt/ serwisu generującego tajne klucze witryny WordPress.org}
 * Klucze te mogą zostać zmienione w dowolnej chwili, aby uczynić nieważnymi wszelkie istniejące ciasteczka. Uczynienie tego zmusi wszystkich użytkowników do ponownego zalogowania się.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'cr=Cg@<dMoeE^~TVT|0Q<Oo|<N<MGku-sb3?bzK4V VncRRS_ho*01kj/Rzv#W%c');
define('SECURE_AUTH_KEY',  '@`$&=Ug&69b:Tq:d/tQ0Wk-X/u9(<U[n%6pWSmjot-{%Hk8-sp{#+d2ot^[p^Ea-');
define('LOGGED_IN_KEY',    'mTFy?H}Nf(+m%id52S=+WJd[)Z)q$N+%1~:/mh++pbaDg-Nck7zy4jcP_34vDJ>r');
define('NONCE_KEY',        'p`vvbG2hr1w=GL/{B*.aC-7[<g,M9<~;jq!Y2ee}}?,[[zK{u`QOg_Wq8?=4XqHc');
define('AUTH_SALT',        'f--R]{6:$/#AzX}(?B^>a*6}}7f-zqIL<q*9.g8vtq7Bn2,K-i&l%>j;9/P^/346');
define('SECURE_AUTH_SALT', '8PO+%b.t6HB9 T2l%;f7JZ(r9E8X>;+lj_0Md-GjJo&5Ca0Wj%K+68*#:P/mXnB^');
define('LOGGED_IN_SALT',   'c)`{t-LF-kNj8z]yD*:R(F}3Wan:dmS#;<)X{8^Sv$o%?+k&S-x|K#10aT$GU3ZX');
define('NONCE_SALT',       'iC81=*@B!VR0ibd=xZF|D4X,.Sm7m*|zkMPKW:<y|Wl:mnd^=6d}QnQ XMJS6f|h');

/**#@-*/

/**
 * Prefiks tabel WordPressa w bazie danych.
 *
 * Możesz posiadać kilka instalacji WordPressa w jednej bazie danych,
 * jeżeli nadasz każdej z nich unikalny prefiks.
 * Tylko cyfry, litery i znaki podkreślenia, proszę!
 */
$table_prefix  = 'wp_';

/**
 * Dla programistów: tryb debugowania WordPressa.
 *
 * Zmień wartość tej stałej na true, aby włączyć wyświetlanie ostrzeżeń
 * podczas modyfikowania kodu WordPressa.
 * Wielce zalecane jest, aby twórcy wtyczek oraz motywów używali
 * WP_DEBUG w miejscach pracy nad nimi.
 */
define('WP_DEBUG', false);

/* To wszystko, zakończ edycję w tym miejscu! Miłego blogowania! */

/** Absolutna ścieżka do katalogu WordPressa. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Ustawia zmienne WordPressa i dołączane pliki. */
require_once(ABSPATH . 'wp-settings.php');


