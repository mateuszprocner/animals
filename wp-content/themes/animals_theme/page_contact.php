<?php
/*
 * Template Name: About page
 * Description: A Page Template for contact
 */

get_header();

while (have_posts()) : the_post();
    ?>
    <div class="col-xs-12 col-sm-8">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="row">
                <div class="col-xs-12">
                    <header class="entry-header">
                        <?php the_title('<h1 class="entry-title"><i class="fa fa-paw fa-fw"></i>', '</h1>'); ?>
                    </header><!-- .entry-header -->
                    <div class="entry-content">

                        <?php // the_content(); ?>
                    </div><!-- .entry-content -->
                    <?php
                endwhile;
                ?>
            </div>
        </div>
        <?php
//    O NAS
        $query = new WP_Query('cat=6');
        $liTemplate = '<li role="presentation" class="%s"><a href="#%s" aria-controls="%s" role="tab" data-toggle="tab"><i class="fa fa-paw fa-fw"></i>%s</a></li>';
        $posts = [];
        $isFirstPost = true;
        if ($query->have_posts()):
            while ($query->have_posts()):
                $query->the_post();
                if ($isFirstPost) {
                    $liClasses = 'active';
                    $isFirstPost = false;
                } else {
                    $liClasses = '';
                }
                $postSlug = 'post-' . $post->ID;
                $postArray['tab'] = sprintf($liTemplate, $liClasses, $postSlug, $postSlug, get_the_title());
                $postArray['content'] = get_the_content();
                $postArray['id'] = $postSlug;
                $posts[] = $postArray;
            endwhile;
        endif;
//    print_r($posts);
        ?>
        <div class="row">

            <div class="col-xs-12">

                <div id="primary" class="content-area">
                    <div role="tabpanel">
                        <ul class="nav nav-tabs nav-justified" role="tablist" id="reader-nav">
                            <?php
                            foreach ($posts as $p) {
                                echo $p['tab'];
                            }
                            ?>
                        </ul>
                        <div class="tab-content">
                            <?php
                            $isFirstPost = true;
                            foreach ($posts as $p) {
                                if ($isFirstPost) {
                                    $postClasses = "active";
                                    $isFirstPost = false;
                                } else {
                                    $postClasses = "";
                                }
                                ?>
                                <div role="tabpanel" class="tab-pane <?= $postClasses; ?>" id="<?= $p['id']; ?>">
                                    <?= $p['content']; ?>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article><!--#post-## -->
</div>

<?php
get_footer();
?>
