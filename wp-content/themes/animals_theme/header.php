<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <link href='http://fonts.googleapis.com/css?family=Marcellus&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Courgette&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <link rel="shortcut icon" href="<?= get_template_directory_uri(); ?>/img/animals_favicon.ico" />

        <!--[if lt IE 9]>
        <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/html5.js"></script>
        <![endif]-->
        <script>(function() {
                document.documentElement.className = 'js'
            })();</script>
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <div id="page" class="hfeed site">

            <div class="fluent-container">
                <div class="page-shadow-container">
                    <header id="site-header"  role="banner">
                        <div class="header-image">
                            <img id="logo-image" src="<?= get_template_directory_uri(); ?>/img/header.png" alt="logo" />
                        </div>
                    </header><!-- .site-header -->
                    <div id="content" class="site-content">
                        <div class="collar"><?php my_navigation(); ?></div>
                        <div id="primary" class="content-area">
                                