/* global screenReaderText */
/**
 * Theme functions file.
 *
 * Contains handlers for navigation and widget area.
 */

(function($) {
  $('#reader-nav a').click(function(e) {
    e.preventDefault();
    $(this).tab('show');
  })

})(jQuery);
